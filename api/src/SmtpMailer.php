<?php
class SmtpMailer {
    protected $url;
    protected $port;
    protected $usersame;
    protected $password;
    protected $from;
    protected $fromname;

    function __construct($url,$port,$username,$password,$from,$fromname) {
        $this->url = $url;
        $this->port = $port;
        $this->username = $username;
        $this->password = $password;
        $this->from = $from;
        $this->fromname = $fromname;

    }

    function create($logger) {
        $mail = new PHPMailer(true);
        $mail->Debugoutput = function ($str, $level) use ($logger) {
            $logger->debug(trim(iconv("UTF-8","UTF8//IGNORE",$str)));
        };
        $mail->SMTPDebug = 3;
        $mail->isSMTP();
        $mail->Host = $this->url;
        $mail->SMTPAuth = true;
        $mail->Username = $this->username;
        $mail->Password = $this->password;
        $mail->SMTPSecure = "tls";
        $mail->Port = $this->port;
        $mail->CharSet = "UTF-8";

        $mail->From = $this->from;
        $mail->FromName = $this->fromname;

        /*$mail->SMTPOptions = array(
           'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );*/

        return $mail;
    }
 } 
