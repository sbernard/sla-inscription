<?php
use \Slim\Middleware\HttpBasicAuthentication\PdoAuthenticator;
use \Slim\Middleware\JwtAuthentication\RequestPathRule;
use \Slim\Middleware\JwtAuthentication\RequestMethodRule;
use \Firebase\JWT\JWT;

function createDataBase ($db) {
    $db->exec('CREATE TABLE IF NOT EXISTS members (
                id INTEGER PRIMARY KEY,
                season INTEGER NOT NULL,
                date INTEGER NOT NULL,
                email VARCHAR(128) NOT NULL,
                name VARCHAR(128) NOT NULL,
                firstname VARCHAR(128) NOT NULL,
                phonenumber VARCHAR(32) NOT NULL,

                yearofbirth INTEGER NOT NULL,
                gender CHAR(1) NOT NULL,
                home VARCHAR(32) NOT NULL,

                leisureVolley BOOLEAN NOT NULL,
                fsgtVolley BOOLEAN NOT NULL,
                leisureFoot BOOLEAN NOT NULL,
                fsgtFoot BOOLEAN NOT NULL,
                futsal BOOLEAN NOT NULL,

                volleyTeamMix1 BOOLEAN NOT NULL,
                volleyTeamMas1 BOOLEAN NOT NULL,
                volleyTeamMas2 BOOLEAN NOT NULL,
                volleyTeamFem1 BOOLEAN NOT NULL,

                footTeam1 BOOLEAN NOT NULL,
                footTeam2 BOOLEAN NOT NULL,
                footTeam3 BOOLEAN NOT NULL,
                footTeam4 BOOLEAN NOT NULL,

                paiement BOOLEAN NOT NULL,
                fsgtLicence BOOLEAN NOT NULL,
                certificate BOOLEAN NOT NULL,
                badge BOOLEAN NOT NULL,
                backBadge BOOLEAN NOT NULL
    )');

    $db->exec('CREATE TABLE IF NOT EXISTS config (
                `key` VARCHAR(32) PRIMARY KEY,
                `value` VARCHAR(128)
    )');

    $db->exec('INSERT INTO config (`key`, `value`) VALUES ("currentSeason", null)');
}

function initApp($key, $db, $mailer, $mailman, $config, $logger, $admins){

    $c = new \Slim\Container($config);
    $app = new \Slim\App($c);

    // Add middleware for security
    $app->add(new \Slim\Middleware\HttpBasicAuthentication([
        "logger" => $logger,
        "path" => ["/members", "/mails"],
        "realm" => "Protected",
        "users" => $admins
    ]));
    $app->add(new \Slim\Middleware\JwtAuthentication([
        "logger" => $logger,
        "secret" => $key,
        "path" => "/",
        "rules" => [
            new RequestPathRule([
                "passthrough" => ["/join", "/install", "/members/*", "/seasons/*","/mails/*"]
            ]),
            new \Slim\Middleware\JwtAuthentication\RequestMethodRule([
                "passthrough" => ["OPTIONS"]
            ])
        ]
    ]));

    // Create and init the database
    $app->get('/install', function ($request, $response, $args) use ($db) {
        createDataBase($db);
        return $response->write("installed :". print_r($db->errorInfo()));
    });

    // Initiate inscription
    $app->post('/join', function ($request, $response, $args) use ($key,$logger,$mailer, $db){

        // get params
        $email = trim($request->getParam("email"));
        $honeypot = $request->getParam("honeypot");

        // check honeypot is not filled
        if (!empty($honeypot)) {
            return $response->withStatus(403);
        }

        // validate email
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
            return $response->withStatus(400)->withJson(array("msg"=>"invalid email"));
        }

        // check inscriptions are opened
        $sql = "SELECT value FROM config WHERE `key` = 'currentSeason'";
        $sth = $db->prepare($sql);
        $sth->execute();
        $currentSeason = $sth->fetchColumn(0);
        if (empty($currentSeason)) {
            return $response->withStatus(400)->withJson(array("msg"=>"inscriptions closed"));
        }

        // check it does not alrealy exist
        $sql = "Select email FROM members WHERE email = :email AND season = :season";
        $sth = $db->prepare($sql);
        $sth->bindParam("email", $email);
        $sth->bindParam("season", $currentSeason);
        $sth->execute();
        $res = $sth->fetchAll();
        if ($res != null)
            return $response->withStatus(400)->withJson(array("msg"=>"email already used"));

        // create token
        $tokenPayload = array(
            "email" => $email,
            "scope" => "joining",
            "date"  => time(),
        );
        $token = JWT::encode($tokenPayload, $key);

        // create email
        $mail = $mailer->create($logger);
        $mail->addAddress($email);
        $mail->Subject = "Inscription SLA (confirmation email)";
        $protocol = "http" . ($_SERVER['HTTPS'] ? 's' : '');
        $host =  $_SERVER['HTTP_HOST'];
        $body = sprintf('Cliquer <a href="%s://%s#join?token=%s">ici</a> pour continuer votre inscription.', $protocol, $host, $token);
        $mail->msgHTML($body);

        // try to send mail
        try{
            $mail->send();
            return $response->withJson(null);
        } catch (phpmailerException $e) {
            $logger->error($e->getMessage());
            return $response->withStatus(500)->withJson(array("msg"=>$e->getMessage()));
        } catch (Exception $e) {
            $logger->error($e->getMessage());
        }
        return $response->withStatus(500)->withJson(array("msg"=>"unable to send email"));
    });

    // new inscription : this will create
    $app->post('/finalize', function ($request, $response, $args) use ($db, $logger, $mailer, $mailman){
        $token = $request->getAttribute("token");

        // check token scope and email presence
        if (!isset($token->scope) || $token->scope != "joining") {
            return $response->withStatus(401)->withJson(array("msg"=>"invalid scope"));
        }

        // check token validity
        $tokenValidity = 86400; //24h
        if (!isset($token->date) || $token->date + $tokenValidity < time()){
            return $response->withStatus(401)->withJson(array("msg"=>"token expired"));
        }

        // check token scope and email precense
        if (!isset($token->email)) {
            return $response->withStatus(401)->withJson(array("msg"=>"invalid token"));
        }
        $email = $token->email;

        // check inscriptions are opened
        $sql = "SELECT value FROM config WHERE `key` = 'currentSeason'";
        $sth = $db->prepare($sql);
        $sth->execute();
        $currentSeason = $sth->fetchColumn(0);
        if (empty($currentSeason)) {
            return $response->withStatus(400)->withJson(array("msg"=>"inscriptions closed"));
        }

        // check it does not alrealy exist
        $sql = "Select email FROM members WHERE email = :email AND season = :season";
        $sth = $db->prepare($sql);
        $sth->bindParam("email", $email);
        $sth->bindParam("season", $currentSeason);
        $sth->execute();
        $res = $sth->fetchAll();
        if ($res != null)
            return $response->withStatus(400)->withJson(array("msg"=>"email already used"));

        // TODO check JSON ?

        // add member
        $json = $request->getParsedBody();
        $sql = "INSERT INTO members
                    (season, date, email, name, firstname, phonenumber,
                     yearofbirth, gender, home,
                     leisureVolley, fsgtVolley, leisureFoot, fsgtFoot, futsal,
                     volleyTeamMix1, volleyTeamMas1, volleyTeamMas2, volleyTeamFem1,
                     footTeam1, footTeam2 , footTeam3, footTeam4,
                     paiement, fsgtLicence, certificate, badge, backBadge)
                VALUES
                    (:season, :date, :email, :name, :firstname, :phonenumber,
                     :yearofbirth, :gender, :home,
                     :leisureVolley, :fsgtVolley, :leisureFoot, :fsgtFoot, :futsal,
                     :volleyTeamMix1, :volleyTeamMas1, :volleyTeamMas2, :volleyTeamFem1,
                     :footTeam1, :footTeam2 , :footTeam3, :footTeam4,
                     :paiement, :fsgtLicence, :certificate, :badge, :backBadge)";
        $sth = $db->prepare($sql);
        $sth->bindValue("season", $currentSeason);
        $sth->bindValue("date", time());
        $sth->bindParam("email", $email);
        $sth->bindParam("name", ucfirst(strtolower(trim($json['name']))));
        $sth->bindParam("firstname", ucfirst(strtolower(trim($json['firstname']))));
        $sth->bindParam("phonenumber", trim($json['phonenumber']));
        $sth->bindParam("yearofbirth", $json['yearofbirth']);
        $sth->bindParam("gender", $json['gender']);
        $sth->bindParam("home", $json['home']);
        $sth->bindParam("leisureVolley", $json['leisureVolley'], PDO::PARAM_BOOL);
        $sth->bindParam("fsgtVolley", $json['fsgtVolley'], PDO::PARAM_BOOL);
        $sth->bindParam("leisureFoot", $json['leisureFoot'], PDO::PARAM_BOOL);
        $sth->bindParam("fsgtFoot", $json['fsgtFoot'], PDO::PARAM_BOOL);
        $sth->bindParam("futsal", $json['futsal'], PDO::PARAM_BOOL);

        $sth->bindValue("volleyTeamMix1", false, PDO::PARAM_BOOL);
        $sth->bindValue("volleyTeamMas1", false, PDO::PARAM_BOOL);
        $sth->bindValue("volleyTeamMas2", false, PDO::PARAM_BOOL);
        $sth->bindValue("volleyTeamFem1", false, PDO::PARAM_BOOL);
        $sth->bindValue("footTeam1", false, PDO::PARAM_BOOL);
        $sth->bindValue("footTeam2", false, PDO::PARAM_BOOL);
        $sth->bindValue("footTeam3", false, PDO::PARAM_BOOL);
        $sth->bindValue("footTeam4", false, PDO::PARAM_BOOL);

        $sth->bindValue("paiement", false, PDO::PARAM_BOOL);
        $sth->bindValue("fsgtLicence", false, PDO::PARAM_BOOL);
        $sth->bindValue("certificate", false, PDO::PARAM_BOOL);
        $sth->bindParam("badge", $json['badge'], PDO::PARAM_BOOL);
        $sth->bindValue("backBadge", false, PDO::PARAM_BOOL);
        $sth->execute();

        // try to add member to mailing lists
        try {
            $mailman->members()->subscribe($email);
        } catch (Services_Mailman_Exception $e) {
                $logger->warning("unable to add " . $email . " to members mailing list :" . $e->getMessage());
        }
        try {
            if ($json['leisureVolley'] || $json["fsgtVolley"]){
                 $mailman->volley()->subscribe($email);
            }
        } catch (Services_Mailman_Exception $e) {
                $logger->warning("unable to add " . $email . " to volley mailing list :" . $e->getMessage());
        }
        try {
            if ($json['leisureFoot'] || $json["fsgtFoot"]){
                 $mailman->foot()->subscribe($email);
            }
        } catch (Services_Mailman_Exception $e) {
                $logger->warning("unable to add " . $email . " to foot mailing list :" . $e->getMessage());
        }
        try {
            if ($json['futsal']){
                 $mailman->futsal()->subscribe($email);
            }
        } catch (Services_Mailman_Exception $e) {
                $logger->warning("unable to add " . $email . " to futsal mailing list :" . $e->getMessage());
        }

        // list subcribed mailing lists
        $mailinglists = array ('<li><a href="mailto:membres@sla-asso.fr">membres@sla-asso.fr</a> : tous les membres de la SLA.</li>');
        if ($json['leisureVolley'] || $json["fsgtVolley"]) {
            array_push($mailinglists, '<li><a href="mailto:volley@sla-asso.fr">volley@sla-asso.fr</a> : tous les joueurs de volley loisir et FSGT.</li>');
        }
        if ($json['leisureFoot'] || $json["fsgtFoot"]) {
            array_push($mailinglists, '<li><a href="mailto:foot@sla-asso.fr">foot@sla-asso.fr</a> : tous les joueurs de foot loisir et FSGT.</li>');
        }
        if ($json['futsal']) {
            array_push($mailinglists, '<li><a href="mailto:futsal@sla-asso.fr">futsal@sla-asso.fr</a> : tous les joueurs de futsal.</li>');
        }

        // create email
        $mail = $mailer->create($logger);
        $mail->addAddress($email);
        $mail->Subject = "Inscription SLA récapitulatif";
        $template =
'
    <p>Votre inscription sera definitivement valider après remise du chèque d\'adhésion
    et du certificat médical(ou attestation) à votre responsable d\'activité
    comme expliqué dans <a href="https://sla-asso.fr/inscription/#procedure">la procédure d\'inscription.</a></p>

    <p>Vous avez été ajouté aux listes de diffusions :
    <ul>
%s
    </ul>
    Envoyez un mail à ces adresses permet de contacter tous les membres d\'une activité.</p>

    <p>Parfois certains hébergeurs mails ont tendances à nous considérer comme du courriers indésirables.
    Donc pensez à vérifier vos courriers indésirables et/ou aller lire <a href=" https://sla-asso.fr/mail-sla">cette page</a> pour avoir plus de détails et savoir comment configurer votre messagerie.</p>

    <p>Voici quelques adresses et liens utiles :
    <ul>
    <li><a href="https://sla-asso.fr/">https://sla-asso.fr/</a> : le site de la SLA.</li>
    <li><a href="http://www.fsgt31.fr/">http://www.fsgt31.fr/</a> : le site de la FSGT.</li>
    <li><a href="mailto:president@sla-asso.fr">president@sla-asso.fr</a> : email du président de la SLA.</li>
    <li><a href="mailto:tresorier@sla-asso.fr">tresorier@sla-asso.fr</a> : email du trésorier de la SLA.</li>
    <li><a href="mailto:secretaire@sla-asso.fr">secretaire@sla-asso.fr</a> : email du secrétaire de la SLA.</li>
    <li><a href="mailto:bureau@sla-asso.fr">bureau@sla-asso.fr</a> : email pour contacter tout le bureau (président, trésorier, secrétaire).</li>
    <li><a href="mailto:contact.foot.fsgt@sla-asso.fr">contact.foot.fsgt@sla-asso.fr</a> : email du responsable de l\'activité foot FSGT.</li>
    <li><a href="mailto:contact.foot.loisir@sla-asso.fr">contact.foot.loisir@sla-asso.fr</a> : email du responsable de l\'activité foot loisir.</li>
    <li><a href="mailto:contact.futsal@sla-asso.fr">contact.futsal@sla-asso.fr</a> : email du responsable de l\'activité futsal.</li>
    <li><a href="mailto:contact.volley@sla-asso.fr">contact.volley@sla-asso.fr</a> : email du responsable de l\'activité volley FSGT et loisir.</li>
    </ul>
    </p>

    <p>Merci et à bientôt sur les terrains !</p>
';
        $mail->msgHTML(sprintf ($template, implode("\n", $mailinglists)));

        // try to send mail
        try {
            $mail->send();
            $mail->smtpClose();
            return $response->withJson(array());
        } catch (phpmailerException $e) {
            $errMsg = $e->errorMessage();
        } catch (Exception $e) {
            $errMsg = $e->getMessage();
        }
        $logger->error($e);

        // if we failed to send the mail remove field in the database.
        $sql ='DELETE FROM members
               WHERE email = :email;';
        $sth = $db->prepare($sql);
        $sth->bindParam("email", $email);
        $sth->execute();

        return $response->withStatus(500)->withJson(array("msg"=>"unable to send email"));
    });

    // get member data of the related token
    $app->get('/mydata', function ($request, $response, $args) use ($db,$logger) {
        $token = $request->getAttribute("token");

        // check token scope and email presence
        if (!isset($token->scope) || $token->scope != "joining") {
            return $response->withStatus(401)->withJson(array("msg"=>"invalid scope"));
        }

        // check token validity
        $tokenValidity = 86400; //24h
        if (!isset($token->date) || $token->date + $tokenValidity < time()){
            return $response->withStatus(401)->withJson(array("msg"=>"token expired"));
        }

        // check token scope and email precense
        if (!isset($token->email)) {
            return $response->withStatus(401)->withJson(array("msg"=>"invalid token"));
        }
        $email = $token->email;

        // Search data from previous seasons
        $sql = "SELECT a.*
                FROM members a
                INNER JOIN (
                    SELECT email, max(season) season
                    FROM members
                    GROUP BY email
                ) b ON a.email = b.email AND a.season = b.season
                WHERE a.email=:email;";
        $sth = $db->prepare($sql);
        $sth->bindParam("email", $email);
        $sth->execute();
        $member = $sth->fetchObject('Member');
        return $response->withStatus(200)->withJson($member);
    });


    // get all members
    $app->get('/members/byseason/{season}', function ($request, $response, $args) use ($db) {
        $sql = "SELECT * FROM members WHERE season=:season";
        $sth = $db->prepare($sql);
        $sth->bindParam("season", $args["season"]);
        $sth->execute();
        $members = $sth->fetchAll(PDO::FETCH_CLASS, 'Member');
        return $response->withStatus(200)->withJson($members);
    });

    // get member by id
    $app->get('/members/{id}', function ($request, $response, $args) use ($db) {
        $sql = "SELECT * FROM members WHERE id=:id;";
        $sth = $db->prepare($sql);
        $sth->bindParam("id", $args["id"]);
        $sth->execute();
        $member = $sth->fetchAll(PDO::FETCH_CLASS, 'Member');
        return $response->withStatus(200)->withJson($member);
    });

    // edit one member
    $app->POST('/members/{id}', function ($request, $response, $args) use ($db,$logger) {
        // get json payload
        $json = $request->getParsedBody();

        // check it
        if (count($json) == 0){
            return $response->withStatus(400)->withJson(array("msg"=>"empty json"));
        }
        unset($json["id"]);
        if (count($json) == 0){
            return $response->withStatus(400)->withJson(array("msg"=>"empty json"));
        }

        // build update request
        $sql = "UPDATE  members SET";
        foreach ($json as $attr => $value) {
            $sql= $sql." ".$attr."=:".$attr.",";
        }
        $sql = rtrim($sql,',');
        $sql = $sql." WHERE id = :id;";


        // set param and execute it
        $sth = $db->prepare($sql);
        $sth->bindParam("id", $args["id"]);
        foreach ($json as $attr => $value) {
            $sth->bindParam($attr, $value,PDO::PARAM_BOOL);
        }
        $sth->execute();

        // get new version of this member
        $sql = "SELECT * FROM members WHERE id=:id;";
        $sth = $db->prepare($sql);
        $sth->bindParam("id", $args["id"]);
        $sth->execute();
        $member = $sth->fetchAll(PDO::FETCH_CLASS, 'Member');
        return $response->withStatus(200)->withJson($member);
    });

    // edit one member
    $app->DELETE('/members/{id}', function ($request, $response, $args) use ($db,$logger) {
        // build delete request
        $sql = "DELETE  FROM members WHERE id=:id";

        // set param and execute it
        $sth = $db->prepare($sql);
        $sth->bindParam("id", $args["id"]);
        $sth->execute();
        return $response->withStatus(200);
    });

    // get current season
    $app->get('/seasons/current', function ($request, $response, $args) use ($db) {
        $sql = "SELECT value FROM config WHERE `key` = 'currentSeason'";
        $sth = $db->prepare($sql);
        $sth->execute();
        $currentSeason = $sth->fetchColumn(0);
        return $response->withStatus(200)->withJson($currentSeason);
    });

    // get all members
    $app->get('/seasons', function ($request, $response, $args) use ($db) {
        // get all season
        $sql = "SELECT season FROM members GROUP BY season ORDER BY season DESC";
        $sth = $db->prepare($sql);
        $sth->execute();
        $seasons = $sth->fetchAll(PDO::FETCH_COLUMN, 0);

        // add current season if needed
        $sql = "SELECT value FROM config WHERE `key` = 'currentSeason'";
        $sth = $db->prepare($sql);
        $sth->execute();
        $currentSeason = $sth->fetchColumn(0);
        if ($currentSeason && !in_array($currentSeason, $seasons)){
            array_push($seasons, $currentSeason);
            rsort($seasons);
        }

        return $response->withStatus(200)->withJson($seasons);
    });

    $app->get('/mails/{season}', function ($request, $response, $args) use ($db, $mailman, $logger) {
        // extract query
        $noDryRun = $request->getUri()->getQuery() === "nodryrun";
        $toRemove= array(
            "members" => array(),
            "foot" => array(),
            "volley" => array(),
            "futsal" => array());

        // clean members mailing list
        // get emails from members
        $sql = "SELECT email FROM members WHERE season=:season";
        $sth = $db->prepare($sql);
        $sth->bindParam("season", $args["season"]);
        $sth->execute();
        $membersFromDB = $sth->fetchAll(PDO::FETCH_COLUMN, 0);

        // remove emails in members mailing which is not in the database
        foreach($toRemove as $listName => $emailsToRemove ) {
            $membersFromMailingList = $mailman->{$listName}()->members()[0];
            foreach ($membersFromMailingList as $email) {
                if (!in_array(strtolower($email), array_map('strtolower', $membersFromDB))) {
                    // there is a big in mailman lib which may lead to duplicate
                    // so we push only if it is not already added
                    if (!in_array($email,$toRemove[$listName])) {
                        array_push($toRemove[$listName],$email);
                    }
                }
            }
        }

        if($noDryRun) {
            // remove email from mailing lists
            foreach($toRemove as $listName => $emailsToRemove) {
                foreach ($emailsToRemove as $email) {
                    $mailman->{$listName}()->unsubscribe($email);
                }
            }
        }
        return $response->withStatus(200)->withJson($toRemove);
    });

    return $app;
}
