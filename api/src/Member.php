<?php
class Member {

    public $id;

    public $season;
    public $date;
    public $email;
    public $name;
    public $firstname;
    public $phonenumber;

    public $yearofbirth;
    public $gender;
    public $home;

    public $leisureVolley;
    public $fsgtVolley;
    public $leisureFoot;
    public $fsgtFoot;
    public $futsal;

    public $volleyTeamMix1;
    //public $volleyTeamMix2;
    public $volleyTeamMas1;
    public $volleyTeamMas2;
    public $volleyTeamFem1;
    public $footTeam1;
    public $footTeam2;
    public $footTeam3;
    public $footTeam4;

    public $paiement;
    public $fsgtLicence;
    public $certificate;
    public $badge;
    public $backBadge;

    function __construct() {
        // HACK we need to change type to boolean because we don't know each PDO driver
        // is used and if datatype is supported.
        // https://stackoverflow.com/questions/14977115/how-can-i-fetch-correct-datatypes-from-mysql-with-pdo

        settype($this->id, "integer");

        settype($this->season, "integer");
        settype($this->date, "integer");
        settype($this->email, "string");
        settype($this->name, "string");
        settype($this->firstname,"string");
        settype($this->phonenumber, "string");

        settype($this->yearofbirth, "integer");
        settype($this->gender, "string");
        settype($this->home,"string");

        settype($this->paiement,"boolean");
        settype($this->fsgtLicence,"boolean");
        settype($this->certificate,"boolean");
        settype($this->badge,"boolean");
        settype($this->backBadge,"boolean");

        settype($this->leisureVolley,"boolean");
        settype($this->fsgtVolley,"boolean");
        settype($this->leisureFoot,"boolean");
        settype($this->fsgtFoot,"boolean");
        settype($this->futsal,"boolean");

        settype($this->volleyTeamMix1,"boolean");
        //settype($this->volleyTeamMix2,"boolean");
        settype($this->volleyTeamMas1,"boolean");
        settype($this->volleyTeamMas2,"boolean");
        settype($this->volleyTeamFem1,"boolean");

        settype($this->footTeam1,"boolean");
        settype($this->footTeam2,"boolean");
        settype($this->footTeam3,"boolean");
        settype($this->footTeam4,"boolean");
    }
}
