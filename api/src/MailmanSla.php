<?php
require_once 'lib/Mailman.php';
require_once 'HTTP/Request2.php';

class MailmanSla {

    protected $members;
    protected $volley;
    protected $foot;
    protected $futsal;

    function __construct($url,$memberspwd,$volleypwd,$footpwd,$futsalpwd) {
        $this->members = new Services_Mailman($url, "membres", $memberspwd, $this->createHttpRequest());
        $this->volley = new Services_Mailman($url, "volley",$volleypwd, $this->createHttpRequest());
        $this->foot = new Services_Mailman($url, "foot", $footpwd, $this->createHttpRequest());
        $this->futsal = new Services_Mailman($url, "futsal", $futsalpwd, $this->createHttpRequest());
    }

    function members() {
        return $this->members;
    }

    function volley() {
        return $this->volley;
    }

    function foot() {
        return $this->foot;
    }

    function futsal() {
        return $this->futsal;
    }

    function createHttpRequest() {
        $request =  new HTTP_Request2();

        // HACK deactivate SSL certificate check because our provider does not have a valid certificate :/
        $request->setConfig(array(
            'ssl_verify_peer'   => FALSE,
            'ssl_verify_host'   => FALSE
        ));
        return $request;
    }
 } 
