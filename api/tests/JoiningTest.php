<?php
use PHPUnit\Framework\TestCase;

class JoiningTest extends LocalWebDbTestCase {

    public function test_filled_honeypot(){
        // add new member
        $data = array('email' => 'mon@email.fr', 'honeypot' => 'dazdzad');
        $this->client->post("/join", $data);

        // check REST response
        $this->assertSame($this->client->response->getStatusCode(),403);
    }

     public function test_invalid_email(){
        // add new member
        $data = array('email' => 'mon@email', 'honeypot' => '');
        $this->client->post("/join", $data);

        // check REST response
        $this->assertSame($this->client->response->getStatusCode(),400);
    }

    public function test_new_member(){
        // add new member
        $token = $this->getJoiningToken('mon@email.fr');
        $member = array(
            'sex' => 'M',
            'name' => 'dupont',
            'firstname' => 'jean',
            'address' => '6 rue du lapin',
            'postcode' => '31400',
            'city' => 'Toulouse',
            'phonenumber' => '0512121212',
            'email' => 'mon@email.fr',
            //'dateofbirth' => '2012-04-23',
            'placeofbirth' => 'Albi'
        );
        $res = $this->client->post("/members", $member, array('HTTP_AUTHORIZATION' => "Bearer ". $token));


        // check REST response
        $this->assertSame($this->client->response->getStatusCode(),200);

        // check database content
        $this->assertEquals(2, $this->getConnection()->getRowCount('members'), "Inserting failed");
        $queryTable = $this->getConnection()->createQueryTable('new_members',
             'SELECT * FROM members'
        );
        $expectedTable = $this->createXmlDataSet("tests/joiningResult.xml")->getTable("new_members");
        $this->assertTablesEqual($expectedTable, $queryTable);
    }

    public function test_new_member_without_token(){
        $res = $this->client->post("/members", array());
        $this->assertSame($this->client->response->getStatusCode(),401);
    }

    public function test_new_member_bad_token(){
        // create token with email
        $tokenPayload = array("scope" => "joining", "date" => time());
        $token = $this->getToken($tokenPayload);

        // try to create member
        $res = $this->client->post("/members", array(), array('HTTP_AUTHORIZATION' => "Bearer ". $token));
        $this->assertSame($this->client->response->getStatusCode(),401);

        // create token with bad scope
        $tokenPayload = array("scope" => "admin", "date" => time(), "email" => "mon@email.fr");
        $token = $this->getToken($tokenPayload);

        // try again ...
        $res = $this->client->post("/members", array(), array('HTTP_AUTHORIZATION' => "Bearer ". $token));
        $this->assertSame($this->client->response->getStatusCode(),401);

        // create expired token
        $tokenPayload = array("scope" => "joining", "date" => time()-100000, "email" => "mon@email.fr");
        $token = $this->getToken($tokenPayload);

        // try again ...
        $res = $this->client->post("/members", array(), array('HTTP_AUTHORIZATION' => "Bearer ". $token));
        $this->assertSame($this->client->response->getStatusCode(),401);
    }

    public function getDataSet()
    {
        return $this->createXMLDataSet('tests/joiningFixture.xml');
    }
}

