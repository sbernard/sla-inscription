<?php
// Settings to make all errors more obvious during testing
error_reporting(-1);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
date_default_timezone_set('UTC');

use There4\Slim\Test\WebTestCase;
use There4\Slim\Test\WebDbTestCase;
use \Firebase\JWT\JWT;
use Monolog\Logger;
use Monolog\Handler\ErrorLogHandler;


define('PROJECT_ROOT', realpath(__DIR__ . '/..'));

require_once PROJECT_ROOT . '/vendor/autoload.php';
require PROJECT_ROOT . '/src/app.php';

// Initialize our own copy of the slim application
class LocalWebDbTestCase extends WebDbTestCase {

    protected $pdo;
    protected $key = "test_key";

    public function getConnection() {
        if ($this->conn === null) {
            $this->conn = $this->createDefaultDBConnection($this->getPDO(), ':memory:');
        }
        return $this->conn;
    }

    public function getSlimInstance() {


        // Create and configure Slim App
        $config = [
            'settings' => [
                'displayErrorDetails' => true,
                'determineRouteBeforeAppMiddleware' => true,
                'displayErrorDetails' => true,
                'addContentLengthHeader' => false,
                'version'         => '0.0.0',
                'debug'           => false,
                'mode'            => 'testing',
                'routerCacheFile' => false],
        ];

        // create logger
        $logger = new Logger("slim");
        $handler = new ErrorLogHandler();
        $logger->pushHandler($handler);

        $app =  initApp($this->key, $this->getPDO(),null, $config, $logger);

        return $app;
    }

    public function getPDO() {
        if ($this->pdo === null){
            $this->pdo = new \PDO('sqlite::memory:');
            createDataBase($this->getPDO());
        }
        return $this->pdo;
    }

    public function getToken($payload){
        return JWT::encode($payload, $this->key);
    }

    public function getJoiningToken($email){
        // create token
        $tokenPayload = array(
            "email" => $email,
            "scope" => "joining",
            "date" => time(),
        );
        return $this->getToken($tokenPayload);
    }
};
