<?php
require_once 'vendor/autoload.php';
require_once 'src/app.php';
require_once 'src/SmtpMailer.php';
require_once 'src/MailmanSla.php';
require_once 'config.php';
require_once 'src/Member.php';

use Monolog\Logger;
use Monolog\Handler\ErrorLogHandler;
use Monolog\Formatter\LineFormatter;

// create logger
$logger = new Logger("slim");
$handler = new ErrorLogHandler();
$handler->setLevel(Logger::WARNING);
$handler->setFormatter(new LineFormatter("%level_name%: %message%",null,true,true));
$logger->pushHandler($handler);

// Create database handler
$db = new PDO($pdoDataSourceName, $pdoUsername, $pdoPassword);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// Create mailer
$mailer = new SmtpMailer($smtpurl,$smptport,$smptusername,$smtppwd,$stmpfrom,$smtpfromname);

// Create mailman handler
$mailman = new MailmanSla($mmurl, $mmmemberspwd, $mmvolleypwd, $mmfootpwd, $mmfutsalpwd);

// Create and configure Slim App
$config = [
    'settings' => [
        'displayErrorDetails' => false,
    ],
];

$app = initApp($JwtKey, $db, $mailer, $mailman, $config, $logger,$admins);
$app->run();
