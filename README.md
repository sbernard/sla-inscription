# sla-inscription

sla-inscription est une application d'inscription en ligne pour l'association SLA + une partie administration pour consulter les inscriptions.

L'application est composée d'une partie back-end écrit en PHP basé sur le framework [slim 3](http://www.slimframework.com/docs/v3/) et d'une partie front-end écrit en javascript principalement basé sur  [riot.js v2.x](https://v3.riotjs.now.sh/v2/fr/) and [bootstrap 4](https://getbootstrap.com/docs/4.5/getting-started/introduction/) (pas à jour 😁).

# backend

Le backend expose un API REST disponible à : "http://mon_domaine/api/"

Les données sont stocké dans une base de donnée composé de 2 tables : 
 - members : contentant les inscriptions (données des membres inscrits)
 - config : contentnant la config "dynamique de l'application" ( la config "statique" se trouve dans le ficher api/config.php)

Niveau securité, pour l'inscription [JWT](https://jwt.io/) est utilisé. Un token est envoyé par mail et ce token est nécessaire pour valider l'inscription (et donc ajouter l'entrée dans la base de donnée)

Pour l'interface d'administration l'authentification basique est utilisé. (login/pwd)

Il est possible de lancer l'application localement avec par exemple : `php -S localhost:8080 -t ./` (lancer à la racine du projet)
Avant ça il faut créer un fichier config.php (config.php.sample sert d'exemple)  
(Il est probablement plus simple d'uiliser sqllite en local)  
La base de donnée peut être créer avec un appel un HTTP GET sur http://mon_domaine/api/install. (ou en executant les requetes dans dans la fonction `createDataBase()` de api/src/app.php)

Les inscriptions sont considérés comme fermé quand la key `currentSeason` dans la table config est `NULL`.
Pour ouvrir les inscriptions par exemple pour l'année 2020-2021, la valeur de `currentSeason` doit être : `2020`.

À l'inscription les membres sont automatiquement inscrits à des mailing lists. La déinscription de fait manuellement par les membres.
Mais comme il semblerait que peu de personne se déincrive manuellement, il est aussi possible de désinscire par lot un fois pas an par exemple via l'api : http://mon_domaine/api/mails/{season} où {season} est la saison de référence. Cette API va supprimer tout les addresses emails qui ne sont pas présentes pour la saison {season}.
Exemple en début de saison 2020-2021 avant d'ouvrir les inscriptions, on peut supprimer tous les emails des mailing liste des personnes qui ne se sont pas inscrit en 2019-2020 (ça laisse 1 an au personne pour ce désincrire manuellement) via l'appel d'API : http://mon_domaine/api/mails/2019. Par défaut l'appel ne supprime pas les emails des listes, il ne fait que lister ce qui aurait été supprimer. Pour réellement supprimier il faut utiliser l'option `nodryrun` : https://mon.domaine/api/mails/2019?nodryrun

# front-end

L'appli d'inscription est disponible à l'URL : http://mon_domaine/#home
L'appli d'administration est disponible à l'URL : http://mon_domaine/#admin

# deploiement 
Le plus simple est surement de recloner le repos dans un répertoire propre et de lancer dans le dossier api/ : 
`composer install --no-dev --no-scripts --optimize-autoloader`
puis de copier coller le code sur le server. 
(attention à avoir un fichier api/config.php bien configuré)
