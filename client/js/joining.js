// Create an observable Object
// see http://riotjs.com/api/observable/#constructor
var joining = riot.observable();

joining.start = function(email) {
    jQuery.ajax({
        type:'POST',
        url:'api/join',
        data:{"email":email},
        success: function(data){
            joining.trigger('startSucceed', email, data);
        },
        error: function(xhr, textStatus, errorThrown){
            var errorDetail = null;
            try {
                errorDetail = JSON.parse(xhr.responseText).msg;
            }catch (e){}
            joining.trigger('startFailed', textStatus, xhr.status, errorThrown, errorDetail);
        }
    });
};


joining.finalize = function (member, token) {
    jQuery.ajax({
        type:'POST',
        url:'api/finalize',
        data:JSON.stringify(member),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        headers:{Authorization : 'Bearer ' + token},
        success: function(data){
            joining.trigger('joinSucceed', data);
        },
        error: function(xhr, textStatus, errorThrown){
            var errorDetail = null;
            try {
                errorDetail = JSON.parse(xhr.responseText).msg;
            }catch (e){}
            joining.trigger('joinFailed', textStatus, xhr.status, errorThrown, errorDetail);
        }
    });
};

joining.mydata = function (token) {
    jQuery.ajax({
        type:'GET',
        url:"api/mydata",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        headers:{Authorization : 'Bearer ' + token},
        success: function(data){
            joining.trigger('mydataSucceed', data);
        },
        error: function(xhr, textStatus, errorThrown){
            var errorDetail = null;
            try {
                errorDetail = JSON.parse(xhr.responseText).msg;
            }catch (e){}
            joining.trigger('mydataFailed', textStatus, xhr.status, errorThrown, errorDetail);
        }
    });
};
