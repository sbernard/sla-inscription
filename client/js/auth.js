// Create an observable Object
// see http://riotjs.com/api/observable/#constructor
var auth = riot.observable();

function setToken(token){
    if (!token){
        $.ajaxSettings.headers = {};
        delete sessionStorage.token;
    }else{
        sessionStorage.token = token;
        $.ajaxSettings.headers = {
            Authorization : 'Bearer ' + token
        };
    }
}


// get credentials from local storage
if (sessionStorage.token){
    setToken(sessionStorage.token);
}

// Try to login with the username, password.
auth.login = function(username, password) {
    $.ajax({
        type:'GET',
        url:'api/token',
        username:username,
        password:password,
        success: function(data){
            var d = JSON.parse(data);
            setToken(d.token);
            auth.trigger('login');
        },
        error: function(xhr, type){
            setToken(undefined);
            auth.trigger('loginFailed');
        }
    });
};

auth.signup = function(email) {
    $.ajax({
        type:'POST',
        url:'api/signup',
        data:{"email":email},
        success: function(){
            // HACK forget basic authentication credentials as we will use token
            // https://bugzilla.mozilla.org/show_bug.cgi?id=1064378
            // https://stackoverflow.com/questions/233507/how-to-log-out-user-from-web-site-using-basic-authentication/32325848#32325848
            $.ajax({
                type:'GET',
                url:'api/token',
                username:'logout',
                success: function(data){
                },
                error: function(xhr, type){
                    if (xhr.status === 401){
                        setToken(null);
                        auth.trigger('signupSucceed');
                    }else{
                        auth.trigger('signupFailed');
                    }
                }
            });
        },
        error: function(xhr, type){
                auth.trigger('signupFailed');
        }
    });
};

// Return true is we are currently logged.
auth.isLogged = function(){
    return  sessionStorage.token;
};

auth.changePassword = function (token, password){
    console.log(token, password);
    $.ajax({
    xhrFields: {
        withCredentials: false
    },
    beforeSend: function (xhr) {
        console.log(xhr);
        xhr.setRequestHeader('Authorization', 'Bearer ' + token);
        console.log(xhr);
    },
        type:'POST',
        url:'api/newpwd',
        data:{"password":password},
        /*headers:{
            'Authorization' : 'Bearer ' + token
        },*/
        success: function(data){
              var d = JSON.parse(data);
              setToken(d.token);
              auth.trigger('login');
        },
        error: function(xhr, type){
                auth.trigger('changePasswordFailed');
        }
    });
};

auth.logout = function(){
    $.ajax({
        type:'GET',
        url:'api/token',
        username:'logout',
        password:'logout',
        success: function(data){
            auth.trigger('logoutFailed');
        },
        error: function(xhr, type){
            if (xhr.status === 401){
                setToken(null);
                auth.trigger('logout');
            }else{
                auth.trigger('logoutFailed');
            }
        }
    });

}
