// Create an observable Object
// see http://riotjs.com/api/observable/#constructor
var members = riot.observable();

members.get = function (season) {
    jQuery.ajax({
        type:'GET',
        url:'api/members/byseason/'+ season,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function(data){
            members.trigger('init', data);
        },
        error: function(xhr, textStatus, errorThrown){
            var errorDetail = null;
            try {
                errorDetail = JSON.parse(xhr.responseText).msg;
            }catch (e){}
            members.trigger('error', textStatus, xhr.status, errorThrown, errorDetail);
        }
    });
};

members.put = function (member) {
    jQuery.ajax({
        type:'POST',
        url:'api/members/'+member.id,
        dataType: "json",
        data:JSON.stringify(member),
        contentType: "application/json; charset=utf-8",
        //headers:{Authorization : 'Bearer ' + token},
        success: function(data){
            members.trigger('updated', data[0]);
        },
        error: function(xhr, textStatus, errorThrown){
            var errorDetail = null;
            try {
                errorDetail = JSON.parse(xhr.responseText).msg;
            }catch (e){}
            members.trigger('error', textStatus, xhr.status, errorThrown, errorDetail);
        }
    });
};

members.delete = function (id) {
    jQuery.ajax({
        type:'DELETE',
        url:'api/members/'+id,
        contentType: "application/json; charset=utf-8",
        //headers:{Authorization : 'Bearer ' + token},
        success: function(data){
            members.trigger('deleted', id);
        },
        error: function(xhr, textStatus, errorThrown){
            var errorDetail = null;
            try {
                errorDetail = JSON.parse(xhr.responseText).msg;
            }catch (e){}
            members.trigger('error', textStatus, xhr.status, errorThrown, errorDetail);
        }
    });
};

