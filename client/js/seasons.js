// Create an observable Object
// see http://riotjs.com/api/observable/#constructor
var seasons = riot.observable();

seasons.current = function () {
    jQuery.ajax({
        type:'GET',
        url:'api/seasons/current',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function(data){
            seasons.trigger('current', data);
        },
        error: function(xhr, textStatus, errorThrown){
            var errorDetail = null;
            try {
                errorDetail = JSON.parse(xhr.responseText).msg;
            }catch (e){}
            seasons.trigger('error', textStatus, xhr.status, errorThrown, errorDetail);
        }
    });
};

seasons.get = function () {
    jQuery.ajax({
        type:'GET',
        url:'api/seasons',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function(data){
            seasons.trigger('seasons', data);
        },
        error: function(xhr, textStatus, errorThrown){
            var errorDetail = null;
            try {
                errorDetail = JSON.parse(xhr.responseText).msg;
            }catch (e){}
            seasons.trigger('error', textStatus, xhr.status, errorThrown, errorDetail);
        }
    });
};
