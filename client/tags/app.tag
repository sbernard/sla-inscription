<!-- application view -->
<app>
    <!-- the current page -->
    <div id='page'></div>

    <script>
        var self = this
        self.mountedPage = null // The current mountedPage
        self.loadedPages = {} // Map to know if the page is already compile

        // Load a page
        load(pageName) {
            // Unmount the current page
            if (self.mountedPage) {
                self.mountedPage.unmount(true)
            }

            // Mount the new page
            if (!self.loadedPages[pageName]) {
                riot.compile('client/tags/pages/' + pageName + ".tag", function() {
                    self.loadedPages[pageName] = true
                    self.mountedPage = riot.mount('div#page',pageName, route.query())[0]
                })
            } else {
                self.mountedPage = riot.mount('div#page',pageName, route.query())[0]
            }
        }

        // Method called each time the route change
        // see http://riotjs.com/api/route/#setup-routing
        route(function(pageName) {
            if (!pageName){
                // default page is home page
                pageName = 'home'
                route(pageName)
            }else{
                self.load(pageName)
                self.update()
            }
        })

        // Start listening the url changes and also exec routing on the current url
        route.start(true)
    </script>
</app>
