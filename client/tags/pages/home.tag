<home>
    <div class="container">
        <form class="form-join-start" onsubmit="{start}" if={initialized}>
            <img src="client/images/logo_SLA.png"></img>
            <h1>Inscriptions S.L.A.</h1>

            <div if={currentSeason}>
                <p if={!startSucceed}>Entrez votre adresse email pour démarrer l'inscription !</p>

                <fieldset>
                    <div class="form-group {has-danger:emailError}" if={!startSucceed}>
                        <input id="email" ref="email" class="form-control {form-control-danger:emailError}"
                             type="text" placeholder="monaddresse@email.fr">
                        <div class="form-control-feedback" if={emailError}>Cet email est invalide.</div>
                        <input ref="honey-pot" type="text" hidden></input>
                    </div>
                    <button type="submit" class="btn btn-block btn-primary btn-join-start" if={!startSucceed && !loading}>Commencer</button>
                    <button class="btn btn-block btn-primary btn-join-start" if={loading}><i class="fa fa-circle-o-notch fa-spin"></i> Attendez ...</button>
                </fiebldset>

                <div class="alert alert-success" role="alert" if={startSucceed}>
                    <h4 class="alert-heading">C'est parti !</h4>
                    <p>Un e-mail vous a été envoyé à <strong>{email}</strong>. Il contient un lien qui va vous permettre de continuer l'inscription.</p>
                    <p>Pensez à vérifier <strong>vos courriers indésirables</strong>.</p>
                    <p class="mb-0">Si vous rencontrez un problème contacter : admin@sla-asso.fr</p>
                </div>

                <div class="alert alert-danger" role="alert" if={startFailed}>
                    <h4 class="alert-heading">Oups!</h4>
                    <p>Impossible de démarrer l'inscription : {textStatus} {statusCode} {errorThrown} {errorDetail} </p>
                    <p>Tentez de réessayez plus tard, si le problème persiste, contactez : admin@sla-asso.fr</p>
                </div>
            </div>

            <div if={!currentSeason}>
                <div class="alert alert-info" role="alert" if={!initializationFailed}>
                    <h4 class="alert-heading">Inscriptions closes</h4>
                    <p>Les inscriptions ne sont pas ouvertes pour le moment. </p>
                    <p>L'ouverture des inscriptions sera annoncée sur <a href="https://sla-asso.fr">https://sla-asso.fr</a>.</p>
                </div>
                <div class="alert alert-danger" role="alert" if={initializationFailed}>
                    <h4 class="alert-heading">Oups!</h4>
                    <p>Impossible de démarrer l'inscription : {textStatus} {statusCode} {errorThrown} {errorDetail} </p>
                    <p>Repassez plus tard, si le problème persiste, contactez : admin@sla-asso.fr</p>
                </div>
            </div>
        </form>
    </div>

    <script>
        var self = this;

        // Initialization : get current season
        self.initialized = false;
        self.currentSeason = null;
        seasons.on('current', function(currentSeason){
            self.initialized = true;
            self.currentSeason = currentSeason;
            self.update();
        });
        seasons.on('error', function(textStatus, statusCode,  errorThrown, errorDetail) {
            self.initializationFailed = true;
            self.textStatus = textStatus;
            self.statusCode = statusCode;
            self.errorThrown = errorThrown;
            self.errorDetail = errorDetail;

            self.initialized = true;
            self.update();
        });
        seasons.current()


        start(e) {
            e.preventDefault();
            self.startFailed = false;
            self.startSucceed = false;

            var email = self.refs.email.value.trim();
            if (! validateEmail(email)){
                self.emailError = true
            }else{
                self.loading = true;
                self.emailError = false
                joining.start(email);
            }
        };

        joining.on('startSucceed', function(email) {
            self.startSucceed = true;
            self.loading = false;
            self.email = email;
            self.update();
        });

        // If signup failed display error message
        joining.on('startFailed', function(textStatus, statusCode,  errorThrown, errorDetail) {
            self.startFailed = true;
            self.loading = false;
            self.textStatus = textStatus;
            self.statusCode = statusCode;
            self.errorThrown = errorThrown;
            self.errorDetail = errorDetail;

            self.update();
        });

        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
    </script>
</home>        
