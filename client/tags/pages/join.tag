<join>
    <div class="container">
        <form class="form-join-finish " onsubmit="{join}">
            <img src="client/images/logo_SLA.png"></img>
            <h1>Inscriptions S.L.A.</h1>

            <div if={opts.token} class="text-md-left">
                <p if={!joinSucceed} class="text-center">Remplissez ce formulaire pour terminer votre inscription.</p>

                <fieldset if={!joinSucceed}>
                    <div class="form-group row {has-danger:lastnameError}">
                        <label for="lastname" class="col-sm-3 col-form-label">Nom*</label>
                        <div class="col-sm-9">
                            <input class="form-control" ref="lastname" id="lastname">
                            <div class="form-control-feedback" if={lastnameError}>Le nom est obligatoire.</div>
                        </div>
                    </div>
                    <div class="form-group row {has-danger:firstnameError}">
                        <label for="firstname" class="col-sm-3 col-form-label">Prénom*</label>
                        <div class="col-sm-9">
                            <input class="form-control" ref="firstname" id="firstname">
                            <div class="form-control-feedback" if={firstnameError}>Le prénom est obligatoire.</div>
                        </div>
                    </div>
                    <fieldset class="form-group row">
                      <div class="col-sm-10">
                        <div class="form-check-inline">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="sexe" ref="sexeH" id="sexe" value="H" checked>
                            Homme
                          </label>
                        </div>
                        <div class="form-check-inline">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="sexe" ref="sexeF" id="sexe" value="F">
                            Femme
                          </label>
                        </div>
                      </div>
                    </fieldset>
                    <div class="form-group row">
                        <label for="phonenumber" class="col-sm-3 col-form-label">Numéro de Téléphone</label>
                        <div class="col-sm-9">
                            <input class="form-control" ref="phonenumber" id="phonenumber">
                        </div>
                    </div>
                    <div class="form-group row {has-danger:yearofbirthError}">
                        <label for="yearofbirth" class="col-sm-3 col-form-label">Année de Naissance (AAAA)*</label>
                        <div class="col-sm-9">
                            <input class="form-control" ref="yearofbirth" id="yearofbirth">
                            <div class="form-control-feedback" if={yearofbirthError}>La date de naissance est obligatoire.</div>
                        </div>
                    </div>
                    <div class="form-group row {has-danger:homeError}">
                      <label for="home" class="col-sm-3 col-form-label">Lieu de domicile*</label>
                      <div class="col-sm-10">
                          <select class="custom-select" ref="home" id="home">
                            <option value="0"selected>...</option>
                            <option value="Auzeville Plaine">Auzeville Plaine</option>
                            <option value="Auzeville Coteaux">Auzeville Coteaux</option>
                            <option value="Sicoval">Sicoval</option>
                            <option value="Hors Sicoval">Hors Sicoval</option>
                          </select>
                          <div class="form-control-feedback" if={homeError}>Le lieu de domicile est obligatoire.</div>
                      </div>
                    </div>

                    <div class="form-group row {has-danger:activityError}">
                        <label for="activity" class="col-sm-12 col-form-label">Activité(s)*</label>
                        <div class="col-sm-12 form-check" each={activities}>
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" checked={choosen} onclick={parent.selectActivity}>
                                {name}
                            </label>
                        </div>
                        <div class="col-sm-12 form-control-feedback" if={activityError}>Vous devez choisir au moins une activité.</div>
                    </div>

                    <fieldset class="form-group row">
                      <label class="col-form-label col-sm-12">Avez-vous un badge pour le gymnase du lycée agricole ?</label>
                      <div class="col-sm-10">
                        <div class="form-check-inline">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="badge" ref="badgeyes" id="badgeyes" value="true">
                            Oui
                          </label>
                        </div>
                        <div class="form-check-inline">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="badge" ref="badgeno" id="badgeno" value="false">
                            Non
                          </label>
                        </div>
                      </div>
                    </fieldset>

                    <button type="submit" class="btn btn-block btn-primary btn-join-terminate" if={!loading}>Terminer</button>
                    <button class="btn btn-block btn-primary btn-join-start" if={loading}><i class="fa fa-circle-o-notch fa-spin"></i> Attendez ...</button>
                </fieldset>
                <div class="alert alert-success text-center" role="alert" if={joinSucceed}>
                    <h4 class="alert-heading">Votre inscription est terminée.</h4>
                    <p>Vous allez recevoir un email de récapitulatif.</p>
                    <p>Pensez à vérifier <strong>vos courriers indésirables</strong>.</p>
                    <h4 class="alert-heading">Merci et à bientôt sur les terrains !</h4>
                </div>

                <div class="alert alert-danger text-center" role="alert" if={joinFailed}>
                    <h4 class="alert-heading">Oups!</h4>
                    <p>Impossible de terminer l'inscription : {textStatus} {statusCode} {errorThrown} {errorDetail} </p>
                    <p>Tentez de réessayer plus tard, si le problème persiste, contactez : admin@sla-asso.fr</p>
                </div>
            </div>

            <div class="alert alert-warning text-center" role="alert" if={!opts.token}>
                <h4 class="alert-heading">Oups!</h4>
                <p>Vous avez besoin de <a href="#home" class="alert-link">valider votre adresse email</a> avant de remplir le formulaire d'inscription.</p>
                <p class="mb-0">Si vous rencontrer un problème contacter : admin@sla-asso.fr</p>
            </div>
        </form>
    </div>

    <script>
        var self = this;

        // initialize actvities available
        self.activities = [
                          {"name":"Volley Loisir", "choosen":false},
                          {"name":"Volley FSGT", "choosen":false},
                          {"name":"Foot Loisir", "choosen":false},
                          {"name":"Foot FSGT", "choosen":false},
                          {"name":"Futsal", "choosen":false}
                        ];

        selectActivity(e) {
            var activity = e.item;
            activity.choosen = !activity.choosen;
        }

        // Initialization : get current season
        joining.on('mydataSucceed', function(mydata){
            if (mydata){
                self.refs.lastname.value = mydata.name;
                self.refs.firstname.value = mydata.firstname;
                self.refs.phonenumber.value = mydata.phonenumber;
                self.refs.yearofbirth.value = mydata.yearofbirth;
                self.refs.home.value = mydata.home;
                self.activities[0].choosen = mydata.leisureVolley;
                self.activities[1].choosen = mydata.fsgtVolley;
                self.activities[2].choosen = mydata.leisureFoot;
                self.activities[3].choosen = mydata.fsgtFoot;
                self.activities[4].choosen = mydata.futsal;
                if (mydata.badge)
                    self.refs.badgeyes.checked=true;
                else
                    self.refs.badgeno.checked = true
                self.refs.badgeyes.value = mydata.badge;

                self.update();
            } else{
                self.refs.badgeno.checked = true;
                self.refs.badgeyes.value = false;
            }
        });
        joining.on('mydataFailed', function(textStatus, statusCode,  errorThrown, errorDetail) {
            self.joinFailed = true;
            self.loading = false;
            self.textStatus = textStatus;
            self.statusCode = statusCode;
            self.errorThrown = errorThrown;
            self.errorDetail = errorDetail;
            if (self.errorDetail === "token expired")
              opts.token = null;
            self.update();

            self.update();
        });
        joining.mydata(opts.token);

        // try to finalize the inscription
        join(e){
            e.preventDefault();
            self.joinFailed = false;
            self.joinSucceed = false;
            if (validateForm()){
                self.loading = true;
                joining.finalize({
                    "name":self.refs.lastname.value,
                    "firstname":self.refs.firstname.value,
                    "phonenumber":self.refs.phonenumber.value,
                    "gender":(self.refs.sexeF.checked)? self.refs.sexeF.value:self.refs.sexeH.value,
                    "yearofbirth":self.refs.yearofbirth.value,
                    "home": self.refs.home.value,
                    "leisureVolley":self.activities[0].choosen,
                    "fsgtVolley":self.activities[1].choosen,
                    "leisureFoot":self.activities[2].choosen,
                    "fsgtFoot":self.activities[3].choosen,
                    "futsal":self.activities[4].choosen,
                    "badge":(self.refs.badgeyes.checked)? true:false,
                },
                opts.token);
            }
        }

        joining.on('joinSucceed', function(data) {
            self.joinSucceed = true;
            self.loading = false;
            self.message = data;
            self.update();
        });

        // If signup failed display error message
        joining.on('joinFailed', function(textStatus, statusCode,  errorThrown, errorDetail) {
            self.joinFailed = true;
            self.loading = false;
            self.textStatus = textStatus;
            self.statusCode = statusCode;
            self.errorThrown = errorThrown;
            self.errorDetail = errorDetail;
            if (self.errorDetail === "token expired")
              opts.token = null;
            self.update();
        });

        function validateForm(){
            self.lastnameError = isBlank(self.refs.lastname.value);
            self.firstnameError = isBlank(self.refs.firstname.value);
            self.yearofbirthError = isBlank(self.refs.yearofbirth.value) || !isYearofBirthValid(self.refs.yearofbirth.value);
            self.homeError = self.refs.home.value == 0;
            self.activityError = ! isActivitiesValid();
            return ! (self.lastnameError || self.firstnameError || self.yearofbirthError || self.homeError || self.activityError);
        }

        function isBlank(str) {
            return (!str || /^\s*$/.test(str));
        }

        function isYearofBirthValid(birth) {
           return (/^(19\d\d|200\d)$/.test(birth));
        }

        function isActivitiesValid() {
          for (var i in self.activities) {
            if (self.activities[i].choosen) return true;
          }
          return false;
        }

    </script>
</join>
