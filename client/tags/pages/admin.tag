<admin>
    <div class="container">
        <div>
            <h1>Membres S.L.A</h1>
            <div class="row">
                <div class="col-md-auto">
                    <div class="alert alert-light" role="alert">
                        Saison : <select class="custom-select" ref="seasonCombo" id="season" onchange={seasonChanged}>
                            <option each={s in seasons} value={s}>{s}-{parseInt(s)+1}</option>
                          </select>
                        <strong> {members.length} inscrits, {filteredSize} affichés.</strong>
                        <button type="button" class="btn btn-outline-secondary btn-sm" title="Export CSV des membres affichés" onclick={exportCSV}>
                            <i class="fa fa-table" aria-hidden="true"></i></button>
                        <button type="button" class="btn btn-outline-secondary btn-sm" title="Envoyer un email aux membres affichés" onclick={mailToAll}>
                            <i class="fa fa-envelope" aria-hidden="true"></i></button>
                    </div>
                </div>
                <div class="col">
                    <div if={textStatus} class="alert alert-warning alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                        <strong>Impossible d'effectuer cette action :</strong> {textStatus} {statusCode} {errorThrown} {errorDetail}.
                    </div>
                </div>
            </div>
        </div>
        <div>
            <table class="table table-sm table-hover">
            <thead class="thead-inverse">
            <tr class="row">
                <th class="col-sm-6 align-middle text-center text-md-left form-inline"">
                <input class="form-control" type="text" placeholder="Filtre par Nom/prénom/email" ref="textFilter" onkeyup={ update }>
                </th>
                <th class="col-sm-1 text-center">Vol Lois<br/>
                <input type="checkbox" ref="leisureVolleyFilter" onclick={ change2StateFilter }>
                </th>
                <th class="col-sm-1 text-center">Vol FSGT<br/>
                <input type="checkbox" ref="fsgtVolleyFilter" onclick={ change2StateFilter }>
                </th>
                <th class="col-sm-1 text-center">Foo Lois<br/>
                <input type="checkbox" ref="leisureFootFilter" onclick={ change2StateFilter }>
                </th>
                <th class="col-sm-1 text-center">Foo FSGT<br/>
                <input type="checkbox" ref="fsgtFootFilter" onclick={ change2StateFilter }>
                </th>
                <th class="col-sm-1 text-center">Futsal<br/>
                <input type="checkbox" ref="futsalFilter" onclick={ change2StateFilter }>
                </th>
                <th class="col-sm-1 text-center">Adhé SLA<br/>
                <input type="checkbox" ref="paiementFilter" onclick={ change3StateFilter }>
                </th>
                <!--th class="col-sm-1 text-center">certif/attes <br/>
                <input type="checkbox" ref="certifFilter" onclick={ change3StateFilter }>
                </th>
                <th class="col-sm-1 text-center">Lic FSGT<br/>
                <input type="checkbox" ref="licenceFilter" onclick={ change3StateFilter }>
                </th-->
            </tr>
            </thead>
            <tbody>
            <tr each={filtered_members()} class="row">
              <td class="col-sm-11 text-center text-md-left">
                  <button class="btn btn-link btn-sm" type="button" data-toggle="collapse" data-target={"#col"+id} aria-expanded="false" aria-controls="collapseExample">
                      <i class="fa fa-plus-square-o" aria-hidden="true"></i>
                  </button>
                  <span>{firstname} {name}</span>
                  (<a href="mailto:{email}">{email}</a>)
                  <div class="collapse" id={"col"+ id}>
                     <div class="card card-body">
                        <strong>Date d'inscription:</strong> {new Date(date*1000).toLocaleString()}</br>
                        <strong>Tél:</strong> {phonenumber}</br>
                        <strong>Activité:</strong>
                        {(leisureVolley?"Volley Loisir, ":" ")+(fsgtVolley?"Volley FSGT, ":" ")+(leisureFoot?"Foot Loisir, ":" ")+(fsgtFoot?"Foot FSGT, ":" ")+(futsal?"Futsal, ":" ")}
                        </br>
                        <strong>Année de naissance:</strong> {yearofbirth} </br>
                        <strong>Domicile:</strong> {home} </br>
                        <strong>Sexe:</strong>{gender} </br>
                        <div>
                        <button type="button" class="btn btn-outline-secondary btn-sm float-right" title="Supprimer cette adhésion" onclick={deleteMember}>
                            <i class="fa fa-trash-o" aria-hidden="true"></i></button>
                        </div>
                     </div>
                  </div>
              </td>
              <td class="col-sm-1 text-center">
                  <input type="checkbox" value="" checked={paiement} onclick={ togglePaiement }>
              </td>
              <!--td class="col-sm-1 text-center">
                  <input type="checkbox" value="" checked={certificate} onclick={ toggleCertificate }>
              </td>
              <td class="col-sm-1 text-center">
                  <input if={ fsgtFoot || fsgtVolley } type="checkbox" value="" checked={fsgtLicence} onclick={ toggleLicence }>
              </td-->
            </tr>
            </tbody>
            </table>
        <div>
    </div>

    <script>
        var self = this;
        self.seasons = [];
        seasons.get();
        seasons.on('seasons', function(data) {
            self.seasons = data;
            self.update();
            self.seasonChanged();
        });
        seasons.on('error', function (textStatus, statusCode, errorThrown, errorDetail) {
            self.textStatus = textStatus;
            self.statusCode = statusCode;
            self.errorThrown = errorThrown;
            self.errorDetail = errorDetail;
            self.update();
        });

        self.members = [];

        self.resetError = function(){
            self.textStatus = self.statusCode = self.errorThrown = self.errorDetail = null;
        }

        members.on('init', function(data) {
            self.members = data;
            self.filteredSize = data.length;
            // init all filter checkbox
            function initCheckBoxFilter(cb){if (cb) cb.readOnly=cb.indeterminate=true;};
            var filters = [self.refs.paiementFilter, self.refs.licenceFilter, self.refs.certifFilter,
                           self.refs.leisureVolleyFilter, self.refs.fsgtVolleyFilter,
                           self.refs.leisureFootFilter, self.refs.fsgtFootFilter, self.refs.futsalFilter]
            for (f of filters){initCheckBoxFilter(f);};

            self.update();
        });

        members.on('updated', function (member) {
            self.members = self.members.map(function(oldmember){
               if (member.id === oldmember.id) return member;
               else return oldmember;
            });

            self.update();
        });

        members.on('deleted', function (id) {
            self.members = self.members.filter(function(member){
               return member.id !== id;
            });
            --self.filteredSize
            self.update();
        });

        members.on('error', function (textStatus, statusCode, errorThrown, errorDetail) {
            self.textStatus = textStatus;
            self.statusCode = statusCode;
            self.errorThrown = errorThrown;
            self.errorDetail = errorDetail;
            self.update();
        });

        seasonChanged(e) {
            selectedSeason = self.refs.seasonCombo[self.refs.seasonCombo.selectedIndex].value;
            members.get(selectedSeason);
        }

        togglePaiement(e) {
          e.preventDefault();
          var member = e.item;
          var partialMember = { "id":member.id, "paiement":!member.paiement};
          self.resetError();
          members.put(partialMember);
        }

        toggleLicence(e) {
          e.preventDefault();
          var member = e.item;
          var partialMember = { "id":member.id, "fsgtLicence":!member.fsgtLicence};
          self.resetError();
          members.put(partialMember);
        }

        toggleCertificate(e) {
          e.preventDefault();
          var member = e.item;
          var partialMember = { "id":member.id, "certificate":!member.certificate};
          self.resetError();
          members.put(partialMember);
        }

        deleteMember(e) {
          e.preventDefault();
          var member = e.item;
          self.resetError();
          if (confirm("L'adhésion pour " + member.firstname + " " + member.name + " va être supprimé ?")) {
            members.delete(member.id);
          }
        }

        filtered_members() {
            self.filtered = self.members.filter(function (m) {
                // readOnly means indeterminate state, so no filter
                var paiement = self.refs.paiementFilter.readOnly || (m.paiement == self.refs.paiementFilter.checked);
                var licence = true;//self.refs.licenceFilter.readOnly || (m.fsgtLicence == self.refs.licenceFilter.checked);
                var certif = true;//self.refs.certifFilter.readOnly || (m.certificate == self.refs.certifFilter.checked);

                var leisureVolley = self.refs.leisureVolleyFilter.readOnly || (m.leisureVolley == self.refs.leisureVolleyFilter.checked);
                var fsgtVolley = self.refs.fsgtVolleyFilter.readOnly || (m.fsgtVolley == self.refs.fsgtVolleyFilter.checked);
                var leisureFoot = self.refs.leisureFootFilter.readOnly || (m.leisureFoot == self.refs.leisureFootFilter.checked);
                var fsgtFoot = self.refs.fsgtFootFilter.readOnly || (m.fsgtFoot == self.refs.fsgtFootFilter.checked);
                var futsal = self.refs.futsalFilter.readOnly || (m.futsal == self.refs.futsalFilter.checked);

                if (paiement && licence && certif && leisureVolley && fsgtVolley && leisureFoot && fsgtFoot && futsal) {
                    if (self.refs.textFilter.value){
                        var f = self.refs.textFilter.value.trim().toLowerCase();
                        return m.firstname.toLowerCase().indexOf(f) != -1 ||
                               m.name.toLowerCase().indexOf(f) != -1      ||
                               m.email.toLowerCase().indexOf(f) != -1     ;
                    } else return true;
                } else return false;
            });
            self.filteredSize = self.filtered.length;

            return self.filtered;
        }

        change3StateFilter(e) {
            var cb = e.target;
            // code to handle 3 states checkbox : see https://css-tricks.com/indeterminate-checkboxes/
            if (cb.readOnly) cb.checked=cb.readOnly=false;
            else if (!cb.checked) cb.readOnly=cb.indeterminate=true;
            self.update();
        }

        change2StateFilter(e) {
            var cb = e.target;
            // intermiate and check only (not check does not exist)
            if (cb.readOnly) {cb.checked=true;cb.readOnly=false;}
            else {cb.readOnly=cb.indeterminate=true;cb.checked=false;}
            self.update();
        }

        mailToAll() {
            var mailto = self.filtered.reduce(function(preval, member){
                return preval + member.email + ",";
            },"mailto:");
            mailto.slice(0,-1);
            window.location = mailto;
        }

        exportCSV(){
            var json = self.filtered;

            var CSV = '';
            var row = "";

            //This loop will extract the label from 1st index of on array
            for (var index in json[0]) {
                //Now convert each value to string and comma-seprated
                row += index + ',';
            }
            row = row.slice(0, -1);

            //append Label row with line break
            CSV += row + '\r\n';

            //1st loop is to extract each row
            for (var i = 0; i < json.length; i++) {
                row = "";

                //2nd loop will extract each column and convert it in string comma-seprated
                for (var index in json[i]) {
                    row += '"' + json[i][index] + '",';
                }

                row.slice(0, row.length - 1);

                //add a line break after each row
                CSV += row + '\r\n';
            }

            //Initialize file format you want csv or xls
            var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
            var blob = new Blob([CSV], {type: "text/csv;charset=utf-8"});
            saveAs(blob, "members-"+new Date().toJSON().slice(0,10)+".csv");
        }
    </script>
</admin>
